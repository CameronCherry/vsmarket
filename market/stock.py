import math
import numpy as np
import matplotlib.pyplot as plt
import string
import random
from datetime import datetime


class VirtStock:

    SPECIALISM_TECHNOLOGY = 1
    SPECIALISM_RETAIL = 2
    SPECIALISM_FINANCE = 3
    # TODO: Add more specialisms.

    def __init__(self, tkr, initial_price: float, drift_params: tuple, volatility_params: tuple, specialism: int,
                 dt: float, initial_market_weighting: float = None):
        """
        :type tkr: str
        :type initial_price: float
        :type drift_params: tuple
        :type volatility_params: tuple
        :type specialism: int
        :type dt: float
        :type initial_market_weighting: float

        :param tkr: Stock ticker. If blank, automatically generated.
        :param initial_price: Price at time 0.
        :param drift_params: Follows LogNormal random walk. Enter as (Initial drift, drift of the drift,
                             volatility of drift).
        :param volatility_params: Follows CIR model, d[Sigma(t)^2] = K*(Theta - [Sigma(t)^2])*dt + Ksi*Sigma(t)*dX,
                                  dX~N(0,dt). Enter as (Initial volatility, K, Theta, Ksi).
        :param specialism:
        :param dt: Time period between all stock price valuations.
        :param initial_market_weighting:
        """
        self.tkr = tkr if tkr is not None else "#" + "".join([random.choice(string.digits) for _ in range(8)])
        self.initial_price = initial_price
        self.drift_params = drift_params
        self.initial_drift = drift_params[0]
        self.volatility_params = volatility_params
        self.initial_volatility = volatility_params[0]
        self.specialism = specialism

        self.history = np.array([self.initial_price])
        # self.previous_price = None
        self.price = initial_price
        self.drift = self.initial_drift
        self.volatility = self.initial_volatility
        self.dt = dt

        self.initial_market_weighting = initial_market_weighting if initial_market_weighting is not None else \
            random.random()

    def __eq__(self, other):
        try:
            return self.tkr == other.tkr
        except:
            return False

    def __str__(self):
        return str(f"Ticker: {self.tkr}, Price: {self.price}, Market Weighting: {self.market_weighting}")

    def __repr__(self):
        return f"{self.__class__.__name__}(tkr={self.tkr}, initial_price={self.initial_price}," \
               f"initial_drift={self.initial_drift}, initial_volatility={self.initial_volatility}," \
               f"specialism={self.specialism}, dt={self.dt}," \
               f"initial_market_weighting={self.initial_market_weighting}):)"

    def elapse_time(self, periods: int):
        for i in range(periods):
            self._elapse_price(self.dt)
            self._elapse_drift(self.dt)
            self._elapse_volatility(self.dt)

    def _elapse_price(self, dt):
        # dS(t) = S(t)*(mu*dt + sigma*dX),dX~N(0,dt)
        dx = np.random.normal(0.0, math.sqrt(dt))
        d_price = self.price * (self.drift * dt + self.volatility * dx)
        self.price = self.price + d_price
        self.history = np.append(self.history, self.price)

    def _elapse_drift(self, dt):
        # dMU(t) = MU(t)*(ALPHA*dt + GAMMA*dX),dX~N(0,dt)
        dx = np.random.normal(0.0, math.sqrt(dt))
        alpha = self.drift_params[1]
        gamma = self.drift_params[2]
        d_drift = self.drift * (alpha * dt + gamma * dx)
        self.drift = self.drift + d_drift

    def _elapse_volatility(self, dt):
        # CIR model, according to basic Heston model
        # d[Sigma(t)^2] = K*(Theta - [Sigma(t)^2])*dt + Ksi*Sigma(t)*dX, dX~N(0,dt)
        # TODO: covariance of rho*dt between dX in vol model and price model
        dx = np.random.normal(0.0, math.sqrt(dt))
        while True:
            k = self.volatility_params[1]  # Rate at which vol approaches long-run variance
            theta = self.volatility_params[2]  # Long-run variance
            ksi = self.volatility_params[3]  # volatility of volatility
            d_sigma2 = k * (theta - math.pow(self.volatility, 2)) * dt + ksi * self.volatility * dx
            if 2 * k * theta < math.pow(ksi, 2):
                print("Invalid volatility parameters. Require 2*k*theta >= ksi^2")
                raise ValueError
            else:
                break
        self.volatility = self.volatility + (-math.sqrt(-d_sigma2) if d_sigma2 < 0 else math.sqrt(d_sigma2))

    def plot_stock(self, file_name: str = None):
        fig, ax = plt.subplots(nrows=1, ncols=1)
        ax.plot(self.history)
        if file_name is None:
            file_name = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")+"_stock_plot.png"
        fig.savefig(file_name)
        plt.close(fig)

    @property
    def total_return(self):
        return self.history[-1] / self.history[0] - 1

    @total_return.setter
    def total_return(self, value):
        self.total_return = value

    @property
    def market_weighting(self):
        return self.initial_market_weighting


def random_stock(dt, specialism=None, tkr=None):

    if specialism is None:
        specialism = random.choices([VirtStock.SPECIALISM_TECHNOLOGY, VirtStock.SPECIALISM_RETAIL,
                                     VirtStock.SPECIALISM_FINANCE], weights=[0.25, 0.35, 0.3])[0]
    if specialism == VirtStock.SPECIALISM_TECHNOLOGY:
        initial_price = random.expovariate(0.08)
        drift_params = (random.uniform(-0.001, 0.03), random.uniform(-0.002, 0.01), random.uniform(0.01, 0.1))
        volatility_params = (random.uniform(0.03, 0.3), random.uniform(0.002, 0.05), random.uniform(0.002, 0.05),
                             random.uniform(0.001, 0.002))
    elif specialism == VirtStock.SPECIALISM_FINANCE:
        initial_price = random.expovariate(0.15)
        drift_params = (random.uniform(-0.002, 0.02), random.uniform(-0.005, 0.01), random.uniform(0.001, 0.005))
        volatility_params = (random.uniform(0.001, 0.05), random.uniform(0.001, 0.002), random.uniform(0.001, 0.05),
                             random.uniform(0.001, 0.002))
    # elif specialism == VirtStock.SPECIALISM_RETAIL:
    else:
        initial_price = random.expovariate(0.1)
        drift_params = (random.uniform(-0.01, 0.008), random.uniform(-0.02, 0.03), random.uniform(0.01, 0.05))
        volatility_params = (random.uniform(0.001, 0.2), random.uniform(0.001, 0.05), random.uniform(0.001, 0.05),
                             random.uniform(0.001, 0.002))
    return VirtStock(tkr, initial_price, drift_params, volatility_params, specialism, dt)


def null_stock(tkr=None):
    # noinspection PyTypeChecker
    return VirtStock(tkr, initial_price=None, drift_params=None, volatility_params=None, specialism=None,
                     dt=None, initial_market_weighting=None)


if __name__ == '__main__':
    vS = VirtStock(tkr="TICKER123", initial_price=100.0, drift_params=(0.5, 0.01, 0.01),
                   volatility_params=(0.16, 0.01, 0.05, 0.001), specialism=VirtStock.SPECIALISM_TECHNOLOGY, dt=0.001)
    # print(vS.price)
    # vS.elapse_time(100)
    # print(vS.price)
    # print(vS.history)
    # # vS.plot_stock()
    # print(vS.total_return)
