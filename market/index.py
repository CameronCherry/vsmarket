from stock import VirtStock
from market import VirtMarket
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime


class VirtIndex:

    def __init__(self, vmarket: VirtMarket, specialism: int):
        """
        :type vmarket: VirtMarket
        :type specialism: int

        :param vmarket: The virtual market object for which stocks will be sourced.
        :param specialism: Specialism of the index; one of the VirtStock specialisms (SPECIALISM_TECHNOLOGY,
               SPECIALISM_RETAIL, and SPECIALISM_FINANCE).
        """
        self.vmarket = vmarket
        self.specialism = specialism
        self.dt = self.vmarket.dt
        self.stock_count = np.size(self.stocks)
        self.history = np.array([self.value])

    @property
    def stocks(self):
        return np.array([s for s in self.vmarket.stocks if s.specialism == self.specialism])

    @property
    def value(self):
        return sum([s.price for s in self.stocks])

    @property
    def history(self):
        return np.array([sum([s.history[i] for s in self.stocks]) for i in range(self.vmarket.periods_elapsed)])

    @history.setter
    def history(self, value):
        self._history = value

    @property
    def weightings(self):
        return dict((s.tkr, s.market_weighting) for s in self.stocks)

    @property
    def time(self):
        return self.vmarket.time

    @property
    def periods_elapsed(self):
        return self.vmarket.periods_elapsed

    def plot_index(self, file_name: str = None):
        fig, ax = plt.subplots(nrows=1, ncols=1)
        ax.plot(self.history)
        if file_name is None:
            file_name = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")+"_index_plot.png"
        fig.savefig(file_name)
        plt.close(fig)

    def get_stock_from_ticker(self, tkr):
        for s in self.stocks:
            if s.tkr == tkr:
                return s
        return None


if __name__ == '__main__':
    vm = VirtMarket(stock_count=100, dt=0.01)
    vi_tech = VirtIndex(vm, specialism=VirtStock.SPECIALISM_TECHNOLOGY)
    vi_retail = VirtIndex(vm, specialism=VirtStock.SPECIALISM_RETAIL)
    vi_finance = VirtIndex(vm, specialism=VirtStock.SPECIALISM_FINANCE)
    vm.elapse_time(300)
    # print(vm.periods_elapsed)
    # print(vi.stocks)
    # print(vi.history)
    # vm.plot_market()
    # vi_retail.plot_index("Retail.png")
    # vi_tech.plot_index("Tech.png")
    # vi_finance.plot_index("Finance.png")
    # vm.stocks[0].plot_stock()
    # print(vi_finance.periods_elapsed)
