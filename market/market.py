import stock
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime


class VirtMarket:

    def __init__(self, stock_count: int, dt: float):
        """
        :type stock_count: int
        :type dt: float

        :param stock_count: Number of stocks (initially) in the market.
        :param dt: Time period between all stock price valuations.
        """
        self.initial_stock_count = stock_count
        self.dt = dt
        self.stocks = np.array([stock.random_stock(dt=dt) for _ in range(stock_count)])
        self.history = np.array([self.value])
        self.periods_elapsed = 0

    def elapse_time(self, periods):
        for j in range(periods):
            for s in self.stocks:
                s.elapse_time(periods=1)
            self.history = np.append(self.history, self.value)
            # TODO: self.history should be property getter
            self.periods_elapsed += 1

    @property
    def value(self):
        return sum([s.price for s in self.stocks])

    @property
    def weightings(self):
        return dict((s.tkr, s.market_weighting) for s in self.stocks)

    @property
    def time(self):
        return self.dt * (len(self.history) - 1)

    def plot_market(self, file_name: str = None):
        fig, ax = plt.subplots(nrows=1, ncols=1)
        ax.plot(self.history)
        if file_name is None:
            file_name = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")+"_market_plot.png"
        fig.savefig(file_name)
        plt.close(fig)

    def get_stock_from_ticker(self, tkr):
        for s in self.stocks:
            if s.tkr == tkr:
                return s
        return None

    def market_shock(self):
        pass


if __name__ == '__main__':
    vm = VirtMarket(stock_count=100, dt=0.01)
    vm.elapse_time(10)
    print(vm.value)
    # vm.plot_market()
    print(f"Most expensive: {max([s.price for s in vm.stocks])}")
